Sale Discount
#############

The sale_discount module adds the ability to enter a percentage discount on
sales lines.

Sale Line
*********

  - Discount: Percentage of discount apply on the unit price.
  - Base Price : Original unit price before discount.
